import React from "react"
import Card from "./Card"
import "./Screen.css"
import { pickRandomData } from "../services/dataService"

class Screen extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            normalCard: pickRandomData(),
            secretCard: pickRandomData()
        }

        this.next = this.next.bind(this)
    }


    next = () => {
        this.setState(
            {
                normalCard: pickRandomData(),
                secretCard: pickRandomData()
            }
        )
    }

    render() {
        return (
            <div className="center">
                <div className="App">
                    <header className="App-header">
                        <div>
                            {/* Rendering normal card */}
                            <Card key={this.state.normalCard.id}
                                type="normal"
                                title={this.state.normalCard.title}
                                desc={this.state.normalCard.description}
                                extra={this.state.normalCard.extra}
                            />
                            {/* Rendering secret card */}
                            <Card key={this.state.secretCard.id}
                                type="secret"
                                title={this.state.secretCard.title}
                                desc={this.state.secretCard.description}
                                extra={this.state.secretCard.extra}
                            />
                        </div>
                    </header>
                </div>
                {/* Rendering next button */}
                <button className="next" onClick={this.next}>→</button>
                {/* Rendering reload button */}
                <button className="reload" onClick={() => window.location.reload()}>↩︎</button>
            </div>
        )
    }
}

export default Screen